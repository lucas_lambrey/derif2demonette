Derif2Demonette
===============

The module
----------

.. automodule:: Derif2Demonette

The :class:`Derif2Demonette` class
----------------------------------

.. autoclass:: Derif2Demonette.Derif2Demonette
    :members:

The standalone script
---------------------
.. autofunction:: Derif2Demonette.main