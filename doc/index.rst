.. derif2demonette documentation master file, created by
   sphinx-quickstart on Sun Oct 25 09:47:41 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to derif2demonette's documentation!
===========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   ./DerifSegmenter.rst
   ./Derif2Demonette.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`