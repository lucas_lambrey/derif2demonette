"""A script that extracts Derif entries and converts them to Demonette entries

The DeriF entries are extracted using :class:`DerifSegmenter`.
For each DeriF entry, the script creates as many Demonette entries as there are
formal analyses.
It is possible to convert only the first analysis of each DeriF entry.
The Demonette entries created denote direct relations between lexemes

.. WARNING::

    Neoclassical composition is not yet implemented

**Global variables**

SOURCE (:py:class:`str`) - source of the morphological data

DERIF_TO_MULTEXT (:py:class:`dict`) - convert Derif's grammatical categories 
into Multext format

DEMONETTE_DIC (:py:class:`dict`) - fields represent a standard Demonette entry

**Classes**

:class:`Derif2Demonette` - converts all DeriF entries in TXT file to Demonette
entries in CSV file

**Functions**

:func:`main()` - extracts Derif data from TXT file, converts to Demonette, 
saves in CSV
"""

import DerifSegmenter
import sys
import csv

# VARIABLES ET CONSTANTES DE MODULE
SOURCE = 'derif'

# Conversion : Derif => Multitext
DERIF_TO_MULTEXT = {
    "ADJ"   : "Afp-s",
    "A*"    : "Afp-s",      # neo
    "NAM"   : "Np-s",
    "NOM"   : "Nc-s",
    "N*"    : "Nc-s",       # neo
    "VERBE" : "Vmn----",
    "V*"    : "Vmn----"     # neo
}

DEMONETTE_DIC = {
    'rid'            : "",          #00
    'fid'            : "",          #01
    'lid1'           : "",          #02
    'graph_1'        : "",          #1
    'ori_graph_1'    : SOURCE,      #2
    'lid2'           : "",          #03
    
    'graph_2'        : "",          #3
    'ori_graph_2'    : SOURCE,      #4
    'cat_1'          : "",          #5
    'ori_cat_1'      : SOURCE,      #6
    'cat_2'          : "",          #7
    'ori_cat_2'      : SOURCE,      #8
    'ori_cple'       : SOURCE,      #9
    
    'type_cstr_1'    : 'suf',       #10
    'cstr_1'         : 'Xiser',     #11
    'ori_cstr_1'     : SOURCE,      #12
    'type_cstr_2'    : '',          #13
    'cstr_2'         : 'X',         #14
    'ori_cstr_1'     : SOURCE,      #15
    
    'complexite'     : 'simple',    #16
    'ori_complexite' : 'nouveau',   #17
    'orientation'    : 'des2as',    #18
    'ori_orientation': 'nouveau',   #19
    
    'SemTy_1'        : "",          #20
    'ori_SemTy_1'    : "",          #21
    'SemTy_2'        : "",          #22
    'ori_SemTy_2'    : "",          #23
    'Sous_SemTy_1'   : "",          #24
    'Sous_SemTy_2'   : "",          #25
    'ori_Sous_SemTy' : "",          #26
    'SemTyRss_1'     : "",          #27
    'SemTyRss_2'     : "",          #28
    'ori_SemTyRss'   : "",          #29
    
    'RelSem_n1'      : "",          #30
    'RelSem_n2'      : "",          #31
    'ori_RelSem'     : "",          #32
    'def_conc'       : "",          #33
    'ori_def_conc'   : "",          #34
    'def_abs'        : "",          #35
    'ori_def_abs'    : "",          #36
}



class Derif2Demonette :
    """Extracts DeriF data from TXT file, converts to Demonette, saves in CSV

    .. WARNING::

        The :mod:`DerifSegmenter` module may be needed
    """
    
    def get_derif_data(self, path):
        """Extracts DeriF data from TXT file in path

        Saves the not fully segmented entries in a CSV file, at the destination:
        path+'_not_segmented'

        .. WARNING::

            The :mod:`DerifSegmenter` module is needed

        **Classes used**

        * :class:`DerifSegmenter`:

            * :func:`extract_from_file`
            * :func:`save_to_csv`

        :param path: path to the TXT file containing DeriF data
        :type path: str

        :returns: dictionaries of DeriF data
        :rtype: list
        """
        segmenter = DerifSegmenter.DerifSegmenter()
        extracted, not_segmented = segmenter.extract_from_file(path)
        
        # Saves entries which were not segmented
        if not_segmented :
            segmenter.save_to_csv(  path.strip('.txt') + '_not_segmented.csv', 
                                    not_segmented,
                                    full_segment=False)
        
        return extracted
    
    
    def get_demonette_dic(self):
        """Creates a demonette entry as a dictionary
        
        Makes a copy of the DEMONETTE_DIC constant

        Global variables used:

        * `DEMONETTE_DIC`

        :returns: dictionary containing all fields of a demonette entry
        :rtype: dict
        """
        return DEMONETTE_DIC.copy()
    
    
    def get_cg_lemma_from_neo_base(self, analysis):
        """Gets grammatical category and lemma from neoclassical base
        
        The lemma is the non-neoclassical form of the base. Thus, it may be
        constituted of several tokens in certain cases.

        :param analysis: a formal analysis from :class:`DerifSegmenter`
        :type analysis: dict

        :returns: two strings (the grammatical category and the lemma)
        :rtype: list
        """
        return analysis['base_cg'].split('=')

    def get_cg_lemma_from_neo_exp(self, analysis):
        """Gets grammatical category and lemma from neoclassical exposant
        
        The lemma is the non-neoclassical form of the exposant. Thus, it may be
        constituted of several tokens in certain cases.

        .. note::

            Not used yet, as neoclassical composition is not yet supported

        :param analysis: formal analysis from :class:`DerifSegmenter`
        :type analysis: dict

        :returns:
            - cg (:py:class:`str`) - the grammatical category of the exposant
            - lemma (:py:class:`str`) - the lemma of the exposant
        """

        s = analysis['base_lemma']

        # Grammatical category between ',' and '='
        cg = s[ s.find(',')+1 : s.find('=') ]

        # Lemma between '=' and ']'
        lemma = s[ s.find('=')+1 : s.find(']') ]
        return cg, lemma

    # Behaviour depends on type of adjunction
    def type_is_conv(self, demon, analysis):
        """Updates conversion related data in demonette entry
        
        Modifies directly the following fields in the demonentte dictionary:
        `'type_cstr_1'` (10), `'cstr_1'` (11), and `'type_cstr_2'` (13)

        :param demon: a demonette entry
        :type demon: dict
        :param analysis: formal analysis from :class:`DerifSegmenter`
        :type analysis: dict
        """

        demon['type_cstr_1'] = 'conv' # 10
        demon['cstr_1'] = 'X'         # 11

        demon['type_cstr_2'] = 'conv' # 13
        
        return

    def type_is_comp(self, demon, analysis) :
        """Updates neoclassical composition related data in demonette entry

        .. WARNING:

            Not yet implemented
        
        Modifies directly the following fields in the demonette dictionary:
        `'type_cstr_1'` (10)

        :param demon: a demonette entry
        :type demon: dict
        :param analysis: formal analysis from :class:`DerifSegmenter`
        :type analysis: dict

        :raise NotImplementedError:
        """
        
        # Find cg and lemma from left neoclassical root from Derif data
        left_cg, left_lemma = self.get_cg_lemma_from_neo_exp(analysis)

        demon['type_cstr_1'] = 'comp' # 10 

        e = NotImplementedError(
            'Neoclassical composition not supported : {},{} < {} ({}).'.format(
                analysis['deriv_lemma'],
                analysis['deriv_cg'],
                analysis['base_lemma'],
                analysis['base_cg']
                )
            )
        e.analysis = analysis
        raise e

    def type_is_pre(self, demon, analysis):
        """Updates prefixation related data in demonette entry
        
        Modifies directly the following fields in the demonette dictionary: 
        `'type_cstr_1'` (10), `'cstr_1'` (11)

        Demonette indicates the base with an 'X' in `'cstr_1'` while DeriF
        does not

        :param demon: a demonette entry
        :type demon: dict
        :param analysis: formal analysis from :class:`DerifSegmenter`
        :type analysis: dict
        """
        demon['type_cstr_1'] = 'pre'                  # 10
        demon['cstr_1'] = analysis['exp_lemma'] + 'X' # 11
        return

    def type_is_suf(self, demon, analysis):
        """Updates suffixation related data in demonette entry
        
        Modifies directly the following fields in the demonette dictionary: 
        `'type_cstr_1'` (10), `'cstr_1'` (11)

        Demonette indicates the base with an 'X' in `'cstr_1'` while DeriF 
        does not

        :param demon: a demonette entry
        :type demon: dict
        :param analysis: formal analysis from :class:`DerifSegmenter`
        :type analysis: dict
        """

        demon['type_cstr_1'] = 'suf'                  # 10
        demon['cstr_1'] = 'X' + analysis['exp_lemma'] # 11
        return

    

    def derif2demonette_dic(self, derif_dic, only_first_ana=False):
        """Converts a Derif entry's analyses to demonette entries

        Directly updates following fields: `'graph_1'` (1), `'graph_2'` (3), 
        `'cat_1'` (5), `'cat_2'` (7)

        Indirectly : `'type_cstr_1'` (10), `'cstr_1'` (11), `'type_cstr_2'` (13)

        **Methods used**
        
        * :func:`get_demonette_dic`
        * :func:`get_cg_lemma_from_neo_base`
        * :func:`type_is_comp`
        * :func:`type_is_conv`
        * :func:`type_is_pre`
        * :func:`type_is_suf`

        **Modules used**

        * :mod:`DerifSegmenter`

            * `cg_sav`

        **Global variables used**

        * `DERIF_TO_MULTEXT`

        :param derif_dic: Derif entry as created by :class:`DerifSegmenter`
        :type derif_dic: dict
        :param only_first_ana: determines the extent of the conversion
            (default is `False`)
                
                * `True`: converts only first analysis in 'formal_analysis'
                * `False`: converts all analyses

        :type only_first_ana: bool

        :returns:

            * demonette_dics (:py:class:`list`) - dictionaries of Demonette 
              entries

            * not_implemented (:py:class:`list`) - dictionaries of formal
              analyses from :class:`DerifSegmenter`
        """

        # Dict : calls the function according to the corresponding key
        switch_type = {  'comp' : self.type_is_comp,
                         'conv' : self.type_is_conv,
                         'pre'  : self.type_is_pre,
                         'suf'  : self.type_is_suf
        }
        
        demonette_dics = []
        not_implemented = []
        
        # Each analysis correspond to a morphological relation
        for ana in derif_dic['formal_analysis']:
            
            demon_entry = self.get_demonette_dic()
            
            demon_entry['graph_1'] = ana['deriv_lemma']              # 1
            demon_entry['cat_1'] = DERIF_TO_MULTEXT[ana['deriv_cg']] # 5
            
            
            # Case : neoclassical root = 'A*', 'N*' or  'V*' in ana['base_cg']
            if any(list(filter( lambda x: x in ana['base_cg'], 
                                DerifSegmenter.cg_sav)
            )) :
                # Get grammatical category and lemma
                cg, lemma = self.get_cg_lemma_from_neo_base(ana)
                
                demon_entry['graph_2'] = lemma              # 3
                demon_entry['cat_2'] = DERIF_TO_MULTEXT[cg] # 7

            # Case : standard
            else :
                demon_entry['graph_2'] = ana['base_lemma']              # 3
                demon_entry['cat_2'] = DERIF_TO_MULTEXT[ana['base_cg']] # 7

            
            # 10, 11, 13 (type and cstr)
            try :
                # Apply specific treatment according to type of relation
                switch_type[ana['exp_type']]( demon_entry, ana )
                demonette_dics.append(demon_entry)

            except NotImplementedError as e :
                not_implemented.append(ana)
            
            # Limit to first analysis only
            if only_first_ana :
                break
        
        return demonette_dics, not_implemented



    def derif2demonette_from_file(self, path, only_first_ana=False):
        """Extracts Derif data from a file, converts it to demonette entries

        **Methods used**

        * :func:`get_derif_data`
        * :func:`derif2demonette_dic`

        :param path: path to the file containing DeriF data
        :type path: str
        :param only_first_ana: determines the extent of the conversion
            (default is `False`)

                * `True`: converts only first analysis in 'formal_analysis'
                * `False`: converts all analyses

        :type only_first_ana: bool

        :returns:
            * converted (:py:class:`list`) dictionaries of Demonette entries
            * not_converted (:py:class:`list`) dictionaries of DeriF formal
              analyses, form :class:`DerifSegmenter`
        """
        
        # Extraction
        derif_dics = self.get_derif_data(path)

        # Conversion
        converted = []
        not_converted = []

        for dic in derif_dics :
            demon_dics, not_implemented = self.derif2demonette_dic( dic, 
                                                                only_first_ana)
            
            converted.extend( demon_dics )
            not_converted.extend( not_implemented )
        
        return converted, not_converted



    # OUTPUT
    def save_to_csv(self, path, dics):
        """Saves a list of dictionaries in a CSV file, each dict as a row
        
        The header of the file contains the fields of the dictionaries.

        The dictionaries have to be have exactly the same fields

        **Modules used**

        * :mod:`csv`:

            * :class:`DictWriter`

        :param path: path to the CSV file to write in
        :type path: str
        :param dics: dictionaries which share the same keys
        :type dics: list
        """

        with open(path, mode='w', encoding='latin1') as output_file:
            # Définit les titres des colonnes
            fieldnames = dics[0].keys()

            csv_writer = csv.DictWriter(output_file, fieldnames=fieldnames, 
                delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL
                )
            
            # Écrit la ligne des titres
            csv_writer.writeheader()
            # Reste des lignes
            csv_writer.writerows(dics)

        return
    


    def read_from_csv(self, path) :
        """Extracts a list of dictionaries form a CSV file
        
        If file not found, will prompt to enter another path to a CSV file.
        
        .. note::
            The CSV file must contain a header with non recurring field names
        
        **Modules used**

        * :mod:`csv`:

            * :class:`DictReader`

        :param path: path to CSV file to read from
        :type path: str

        :returns: dictionaries sharing all the same keys (from header)
        :rtype: list
        """

        demon_dics = []
        while(True):
            try :
                with open(path, 'r', encoding = 'latin1') as demon_file :
                    reader = csv.DictReader(demon_file)
                    for row in reader :
                        demon_dics.append(row)

            except FileNotFoundError :
                print("Le fichier '{}' est introuvable.".format(path))
                path = input("Entrer le nom du fichier CSV à lire : ")
                continue

        return demon_dics



def main():
    """Extracts Derif data from TXT file, converts to Demonette, saves in CSV
    
    The function does not require arguments, but two from the command line:

    * Path to the file
    * A boolean that determines the extent of the conversion

        * 'False', 'false', '0', '' : converts all analyses of each DeriF entry
        * Else :  Converts only first analysis of each DeriF entry

    :param path: path to the TXT file to convert
    :type path: str
    :param only_first_ana: determines the extent of the conversion
    :type only_first_ana: str
    """

    # stores command line arguments to dict
    arguments = dict(zip( ['name', 'derif_path', 'only_first_ana'], sys.argv))
    
    derif2demonette = Derif2Demonette()

    # Nothing entered
    if 'only_first_ana' not in arguments :
        arguments['only_first_ana'] = False
    # 'False' or 'false' or '0'
    elif arguments['only_first_ana'] in ['False', 'false', '0']:
        arguments['only_first_ana'] = False
    # Everything else
    else :
        arguments['only_first_ana'] = bool(arguments['only_first_ana'])
    
    # Extraction and conversion
    conv, not_conv = derif2demonette.derif2demonette_from_file(
        arguments['derif_path'], 
        arguments['only_first_ana']
        )
    
    # Saving data
    conv_path = arguments['derif_path'].strip('.txt') + '_demonette.csv'
    not_conv_path = arguments['derif_path'].strip('.txt') + \
                                '_demonette_not_converted.csv'
    
    # converted
    derif2demonette.save_to_csv( conv_path, conv )

    # not converted analyses
    if not_conv :
        print( "{} analyses n'ont pas été converties.".format(len(not_conv) ) )
        derif2demonette.save_to_csv( not_conv_path, not_conv )
    else :
        print("Toutes les analyses ont été converties")
    return



if __name__ == "__main__":
    # execute only if run as a script
    main()
