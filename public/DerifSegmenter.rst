DerifSegmenter
==============

The module
----------

.. automodule:: DerifSegmenter

The :class:`DerifSegmenter` class
---------------------------------

.. autoclass:: DerifSegmenter.DerifSegmenter
    :members:

The standalone script
---------------------
.. autofunction:: DerifSegmenter.main