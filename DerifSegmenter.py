"""A script that segments Derif entries from a TXT file, and stores the data in
dictionaries.

The dictionaries can be stored into a CSV file, which can then be read.
A class is provided for use in other scripts.

**Global variables**

deriv_type (:py:class:`list`) - DeriF's types of adjunction

cg_std (:py:class:`list`) - DeriF's standard grammatical categories

cg_sav (:py:class:`list`) - DeriF's neoclassical grammatical categories

cg_base (:py:class:`list`) - DeriF's grammatical categories

fas_sep (:py:class:`str`) - DeriF's character to delimit formal analyses

fa_aff_base_sep (:py:class:`str`) - DeriF's character to delimit an affix from a
base in a formal analysis

std_sep (:py:class:`str`) - DeriF's standard character to delimit elements

entry_seps (:py:class:`list`) - Derif's strings to delimit derivate, formal 
analysis and paraphrase

entry_fields (:py:class:`list`) - fields of the dictionaries for a DeriF entry

fa_fields (:py:class:`list`) - fields of the dictionaries for formal analyses

**Classes**
:class:`DerifSegmenter` - opens a TXT file, segment DeriF data into dict, store
information in a CSV file

**Functions**

:func:`main()` - segments DeriF data from a file, then saves the result in a CSV
file
"""

import sys # used in main()
import re
import csv


# DeriF
deriv_type = ['suf', 'pre', 'conv', 'comp']

cg_std = ['VERBE','ADJ','NOM','NAM']
cg_sav = ['N*', 'V*', 'A*']

cg_base = cg_std + cg_sav

# Separators
fas_sep = ','
fa_aff_base_sep = '+'

std_sep = '/'
entry_seps = ['==>\t', '"']

# Data fields
entry_fields = ['entry_lemma', 'entry_cg', 'paraphrase', 
                'family_size', 'formal_analysis']
fa_fields = ['exp_cg_from', 'exp_lemma', 'exp_type', 'exp_cg_to', 
                'base_lemma', 'base_cg', 'deriv_lemma', 'deriv_cg']



class DerifSegmenter :
    """Opens TXT file, segments Derif data into dict, stores them in CSV file
    """

    def get_deriv_analysis_para(self, entry):
        """Segments a derif entry in three strings
        
        The three srtings match three parts of a DeriF entry:
        
        * deriv : informations about the derivate
        * analysis : all formal analyses
        * para : the paraphrase 

        **Global variables used**

        * `entry_seps`, separators for the main parts of the DeriF entry

        :param entry: DeriF entry
        :type entry: str

        :returns: three strings for deriv, formal analysis and paraphrase
        :rtype: list
        """
        return re.split('|'.join(entry_seps), entry.strip('"\n'))



    def get_ana_data(self, formal_analysis):
        """Segments the "formal analysis" part of a DeriF entry.

        The subparts correspond to `family_size` and one or several formal
        analyses.

        The segmentation uses the fact that all individual formal analyses all
        end with a grammatical category.

        The information from each of these individual formal analyses is 
        segmented and then stored in a dictionary.


        **Global variables used**

        * `fas_sep`, separator for the individual formal analyses
        * `cg_std`, grammatical categories (used for segmentation)
        * `fa_aff_base_sep`, separator of the affix and base parts in an analysis
        * `std_sep`, standard separator
        * `fa_fields`, fields of the output dictionary

        :param formal_analysis: contains an int and multiple formal analyses
        :type formal_analysis: str

        :returns:
            * family_size (:py:class:`str`), the number of lexemes in relation 
              in this entry
            * analyses_dics (:py:class:`list`), dictionaries for each formal
              analysis between two lexemes in relation in the entry

        :raise ValueError: when the formal analyses are not well segmented
        """

        # Using a regex to segment the analyses :
        
        # Splits whenever there is a category from cg_std or a digit before the
        # character in fas_sep
        delimiters = ['\d'] + cg_std.copy()
        # To check that X is before Y, we use : '(?<=Y)X'
        regex = '(?<=' + (')'+fas_sep+'|(?<=').join(delimiters) + ')' + fas_sep
        analyses = re.split(regex, formal_analysis)
        
        
        analyses_dics = []
    	
        family_size = int(analyses[0])

        # Case : successful segmentation
        if family_size==len(analyses):
            
            for analysis in analyses[1:]:
                
                # Separate information about exposant from information about base
                exp, base = analysis.split(fa_aff_base_sep)
                
                data = exp.split(std_sep) + base.split(std_sep)

                # Stores in dictionary, with the fields in fa_fields
                dic = dict(zip(fa_fields, data))
                
                analyses_dics.append(dic)
            
            return str(family_size), analyses_dics
        
        # Case : failed segmentation
        else :
            e = ValueError(
                "Segmentation of formal analyses failed: expected {} analyses, found {}.".format(
                    family_size-1, len(analyses)-1
                )
            )
            
            e.family_size = str(family_size)
            e.analysis = formal_analysis 
            raise e



    def extract_derif_entry(self, entry):
        """Extracts data from a single DeriF entry, returns a dictionary.

        The output dictionary contains two more fields than the corresponding
        DeriF entry, as it lists information about the derivate of the relation
        that is encoded thanks to the fields: `'deriv_lemma'` and `'deriv_cg'`.

        **Methods used**

        * :func:`get_deriv_analysis_para`
        * :func:`get_ana_data`

        **Global variables used**

        * `std_sep` for segmentation
        * `entry_fields` for the entries of the output dictionary

        :param entry: DeriF entry
        :type entry: str

        :returns: dictionary containing the segmented data from the Derif entry
        :rtype: dict
        
        :raise ValueError: when the formal analyses are not well segmented
        """
  
        dic = dict()
        data = []

        deriv, formal_ana, paraph = self.get_deriv_analysis_para(entry)

        # entry_lemma, entry_cg
        data = deriv.split(std_sep)
        # paraphrase
        data.append( paraph )
          	
        try:
            # family_size, formal_analysis
            data.extend( self.get_ana_data(formal_ana) )

            # dictionary
            dic = dict(zip(entry_fields, data))

        except ValueError as value_error:
            
            # family_size
            data.append( value_error.family_size )
            # formal_analysis (str, not segmented)
            data.append( value_error.formal_analysis )

            dic = dict(zip(entry_fields, data))

            value_error.dic = dic
            raise value_error
        
        else :
            # Adding 'deriv_lemma' and 'deriv_cg' to the analyses' fields
            
            # First analysis : data from 'entry_lemma' and 'entry_cg'
            dic['formal_analysis'][0]['deriv_lemma'] = dic['entry_lemma']
            dic['formal_analysis'][0]['deriv_cg'] = dic['entry_cg']
            
            # Other analyses: data from previous ana's 'base_lemma', 'base_cg'
            for i, ana in enumerate(dic['formal_analysis']):
                
                # Avoiding first analysis
                if i==0:
                    continue

                ana['deriv_lemma'] = dic['formal_analysis'][i-1]['base_lemma']
                ana['deriv_cg']    = dic['formal_analysis'][i-1]['base_cg']
        
        return dic



    def extract_derif_entries(self, entries):
        """Extracts data from several entries, returns two lists of dictionaries

        The first list corresponds to the successfully extracted entries; the
        second one to the incorrectly segmented, flagged by a ValueError.
        
        **Methods used**

        * :func:`extract_derif_entry`

        :param entries: a list of DeriF entries, represented as strings
        :type entries: list

        :returns:
            - extracted (:py:class:`list`) - dictionaries of extracted entries
            - fa_not_segmented (:py:class:`list`) - dictionaries of DeriF that
              were not segmented
        """

        extracted = []
        fa_not_segmented = []

        for entry in entries :
            
            try :
                extracted.append( self.extract_derif_entry(entry) )
            
            except ValueError as value_error :
                fa_not_segmented.append( value_error.dic )
        
        return extracted, fa_not_segmented



    def extract_from_file(self, path):
        """Extracts DeriF data from file, returns two lists of dictionaries

        The first list corresponds to the successfully extracted entries; the
        second one to the incorrectly segmented.
        
        Must open a TXT file. If not found, will ask another file.

        **Methods used**

        * :func:`extract_derif_entries`

        :param path: path to the file that stores DeriF data
        :type path: str

        :returns:
            - (:py:class:`list`) - dictionaries for the segmented entries
            - (:py:class:`list`) - dictionaries for the entries not segmented
        """

        # Open file
        derivatives = []

        while(True):

            try :

                with open(path, 'r', encoding='latin1') as derif:
                    derivatives = derif.readlines()
                break

            except FileNotFoundError:
                # If file not found, ask for another path
                print("Le fichier '{}' est introuvable.".format(path))
                path = input("Entrer le nom du fichier TXT à lire : ")
                continue
        
        # Extracting data
        return self.extract_derif_entries(derivatives)

    # OUTPUT

    def save_to_csv(self, path, dics, full_segment=True):
        """Saves extracted data to CSV. Two behaviours depending on segmentation
        
        Adjusts the header when the data is fully segmented : repeats the fields
        of the formal analyses (`fa_fields`)

        Otherwise, uses only fields in `entry_fields`

        **Modules used**

        * :mod:`csv`

            * :class:`writer`

        **Global variables used**

        * `entry_fiels` for the header of the CSV file
        * `fa_fields` for the header of the CSV file

        :param path: path to file in which to write
        :type path: str
        :param dics: dictionaries containing segmented data from DeriF
        :type dics: list
        :param full_segment: determines the structure of the CSV file
            (default is `True`)

                * `True`: `formal_analysis` is fully segmented
                * `False`: `formal_analysis` is not segmented
                
        :type full_segment: bool
        """

        # Maximal number of analyses influences the header
        
        max_nb_fa = int( max(
            dics,
            key = lambda x: int(x['family_size'])
        )['family_size'] )
        
        rows = []

        # Header
        header = entry_fields.copy()

        # Fully segmented : remove 'formal_analysis' field
        #                   repeats fa_fields as many times as needed
        if full_segment :
            header = header[:-1] + ( fa_fields.copy() * (max_nb_fa-1) )
        
        rows.append(header)
        
        # Rest of the lines
        for dic in dics :

            row = list(dic.values())[:-1]
            
            if not full_segment :
                # appends string of the formal analysis
                row.append( dic[-1] )
            
            else :
                
                for ana in dic['formal_analysis'] :
                    row.extend( list(ana.values()) )

            rows.append( row )
        
        with open(path, mode='w', encoding = "latin1") as output_file:
            csv_writer = csv.writer( output_file, delimiter=',', quotechar='"', 
                quoting=csv.QUOTE_MINIMAL)
            csv_writer.writerows(rows)
        
        return

    def read_from_csv(self, path):
        """Reads DeriF data extracted with :class:`DerifSegmenter` in a CSV file
        
        Must open a CSV file. If not found, will ask another file.

        Handles both fully segmented DeriF entries and not fully segmented.

        **Modules used**

        * :mod:`csv`

            * :class:`reader`

        **Global variables used**

        * `entry_fiels` for the fields of the dictionaries
        * `fa_fields` for the fields of the dictionaries

        :param path: CSV file to read
        :type path: str

        :returns: dictionaries for all entries extracted from CSV
        :rtype: list
        """

        rows = []

        while(True):

            try :
                # Opens file
                with open(path, 'r', encoding='latin1') as derif:
                    rows = csv.reader(derif, delimiter=',')
                
                    dics = []
                    header =[]
                    
                    for line_nb, row in enumerate(rows) :
                        
                        # Finds header
                        if line_nb==0 :
                            header = row
                            continue
                        
                        # Other lines
                        else :
                            
                            # Extract entry data
                            dic = dict( zip(entry_fields[:-1], row) )
                        
                            # Extract formal analysis data

                            # Case : analysis not segmented
                            if len(header) == len(entry_fields) :
                                dic[ entry_fields[-1] ] = row[-1]
                            
                            # Case : segmentation
                            else :
                                
                                analyses = []
                                
                                # keep only data related to formal analysis
                                # -1: because of the extra field in entry_fields
                                row = row[ len(entry_fields[:-1]) : ]
                                
                                # extract data
                                while(row != []) :

                                    analyses.append( 
                                        dict(zip(fa_fields, row)) 
                                        )
                                    row = row[ len(fa_fields) : ]

                                dic[ entry_fields[-1] ] = analyses
                            
                            dics.append(dic)
                break

            except FileNotFoundError :
                print("Le fichier '{}' est introuvable ".format(path))
                path = input("Entrer le nom du fichier CSV à lire : ")
                continue

        return dics



def main():
    """Segments DeriF data from a TXT file, then saves the result in CSV files
    
    The path to the TXT file may be passed as first argument when the script is
    called through the command line (or will be asked when calling the method to
    extract the data).

    The extracted information is stored in two CSV files, which names are
    derived from the path of the source TXT file:

    * path + '.csv': for the correctly extracted entries
    * path + '_no_seg.csv': for the entries not correctly extracted

    **Classes used**

    * :class:`DerifSegmenter` to extract DeriF data and save it in a CSV file

        * :func:`extract_from_file`
        * :func:`save_to_csv`
    

    :param path: path to the TXT file to segment
    :type path: str
    """
    
    # stores command line arguments to dict
    arguments = dict(zip( ['name', 'derif_path'], sys.argv))
    
    segmenter = DerifSegmenter()
    
    # Extraction
    extracted, not_seg = segmenter.extract_from_file( arguments['derif_path'] )
    
    # Saves to files
    extracted_path = arguments['derif_path'].strip('.txt') + '.csv'
    not_seg_path = arguments['derif_path'].strip('.txt') + '_no_seg.csv'
    
    segmenter.save_to_csv( extracted_path, extracted )
    if not_seg :
        segmenter.save_to_csv( not_seg_path,
            not_seg, 
            full_segment=False )
    else :
        print("Aucune erreur lors de l'extraction.")
    return

if __name__ == "__main__":
    # execute only if run as a script
    main()
