Derif2Demonette
===============

Derif2Demonette is a python script that converts [DériF](https://www.ortolang.fr/market/tools/derif) linear entries in a TXT file to [Demonette](https://www.ortolang.fr/market/lexicons/demonette) entries in a CSV files. This is part of the [Demonext project](https://www.demonext.xyz/).

It comprises two files : `Derif2Demonette.py` and `DerifSegmenter.py`.

Not all Demonette morphological relations are created in the resulting file: only direct, ascending relations are implemented, excluding composition.

Requirements
------------
- No parasynthesis in the DériF entries

Installation
------------
Simply download the files (the two are required for the conversion) in the same folder.

Usage
-----
The scripts can be used directly from the command line or used in another python script (Please read the files' documentation to know the expected behaviour).
Rudimentary input/output commands are implemented for usage from command line:
- To locate the TXT file containing the DériF entries
- To specify the name of the CSV file created to store the Demonette entries

Roadmap
-------
There are a few tasks that need implementation:
- Handling of the _composition_ morphological process in `Derif2Demonette.py`
- Handling of _parasynthesis_ processes (where several affixes are adjoined at once) in `DerifSegmenter.py`
- Implementing the reversion of the morphological relations encoded
- Implementing the creation of non direct morphological relations (in the Demonette sense)